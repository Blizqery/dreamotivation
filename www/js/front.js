/**
 * Created by Ladislav on 12. 3. 2015.
 */
var elementPosition = $('#navigation').offset();

$(window).scroll(function(){
    if($(window).scrollTop() > elementPosition.top){
        $('#navigation').addClass('navbar-fixed-top');
        $('#content').css('margin-top', '85px');
    } else {
        $('#navigation').removeClass('navbar-fixed-top');
        $('#content').css('margin-top', '0px');
    }
});
