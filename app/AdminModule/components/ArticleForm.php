<?php
/**
 * Created by PhpStorm.
 * User: Ladislav
 * Date: 19. 2. 2015
 * Time: 17:34
 */

namespace App\Controls;

use Nette,
    Nette\Application\UI\Form;

class ArticleForm extends Nette\Object {



    // ---------- Variables ------------
    private $articleModel;
    private $categoryModel;
    private $names = [];
    private $categories;
    private $presenter;

    // ------------ Init ---------------
    public function __construct(\App\AdminModule\Model\ArticleModel $articleModel, \App\AdminModule\Model\CategoryModel $categoryModel)
    {
        $this->articleModel  = $articleModel;
        $this->categoryModel = $categoryModel;

        $this->categories = $this->categoryModel->getAllCategories();

        foreach($this->categories as $category)
        {
            $this->names[$category->id_article_category] = $category->name;
        }

    }

    // ------------ Form ---------------
    public function create($name, $presenter)
    {
        $this->presenter = $presenter;

        $form = new Form();

        $form->addText('title', "Title")
            ->setAttribute('placeholder', 'Article title')
            ->setRequired();

        $form->addTextArea('summary', "Summary")
            ->setRequired();

        $form->addTextArea('text', 'Content')
            ->setAttribute('class', 'ckeditor')
            ->setRequired();

        $form->addText('source', 'Source link');                            //TODO Regexp
        $form->addText('source_name', 'Source name');

        $form->addSelect('article_category_id', 'Category', $this->names);

        $form->addHidden('author', 'name')
            ->setDefaultValue($name);

        $form->addSubmit('submit', 'Create')
            ->getControlPrototype()
            ->onClick('CKEDITOR.instances["'.$form['text']->getHtmlId().'"].updateElement()');

        $form->onSuccess[] = array($this, 'categoryFormSuccess');
        $form->addProtection([$message = "Not allowed"], [$timeout = NULL]);
        return $form;
    }

    // ---------- On submit -------------
    public function categoryFormSuccess($form, $values)
    {
        foreach($this->categories as $category) {
            if($category->name == $values->article_category_id) {
                $values->article_category_id = $category->article_category_id;
            }
        }

        if(!$this->articleModel->createArticle($values)) {
            $this->presenter->flashMessage('Article not created.','warning');
            $this->presenter->redirect('Article:');
        } else {
            $this->presenter->flashMessage('Article successfully created.','success');
            $this->presenter->redirect('Article:');
        }
    }
} 