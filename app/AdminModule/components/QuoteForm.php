<?php
/**
 * Created by PhpStorm.
 * User: Ladislav
 * Date: 18. 2. 2015
 * Time: 20:03
 */

namespace App\Controls;

use Nette,
    Nette\Application\UI\Form;

class QuoteForm extends Nette\Object {


    // ---------- Variables ------------
    private $quoteModel;
    private $presenter;

    // ------------ Init ---------------
    public function __construct(\App\AdminModule\Model\QuoteModel $quoteModel)
    {
        $this->quoteModel = $quoteModel;
    }

    // ------------ Form ---------------
    public function create($presenter)
    {
        $this->presenter = $presenter;

        $form = new Form();

        $form->addTextArea('text', "Text")
            ->setAttribute('placeholder', 'Quote text');

        $form->addText('author', "Author")
            ->setAttribute('placeholder', 'Quote author');

        $form->addSubmit('submit', "Create");

        $form->onSuccess[] = array($this, 'quoteFormSuccess');
        $form->addProtection([$message = "Not allowed"], [$timeout = NULL]);
        return $form;
    }

    // ---------- On submit -------------
    public function quoteFormSuccess($form, $values)
    {
        if(!$this->quoteModel->createQuote($values)) {
            $this->presenter->flashMessage("There was an error during quote creation, quote not created", 'warning');
            $this->presenter->redirect('this');
        } else {
            $this->presenter->flashMessage("Quote successfully created.", 'success');
            $this->presenter->redirect('this');
        }
    }
} 