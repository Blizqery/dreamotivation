<?php
/**
 * Created by PhpStorm.
 * User: Ladislav
 * Date: 18. 2. 2015
 * Time: 19:17
 */

namespace App\Controls;

use Nette,
    Nette\Application\UI\Form;

class AchievementForm extends Nette\Object {



    // ---------- Variables ------------
    private $achievementModel;
    private $presenter;

    // ------------ Init ---------------
    public function __construct(\App\AdminModule\Model\AchievementModel $achievementModel)
    {
        $this->achievementModel = $achievementModel;
    }

    // ------------ Form ---------------
    public function create($presenter)
    {
        $this->presenter = $presenter;

        $form = new Form();

        $form->addText('name', "Name")
            ->setAttribute('placeholder', 'Name of achievement')
            ->setRequired();

        $form->addTextArea('describe', "Description")
            ->setAttribute('placeholder', 'Description of achievement')
            ->setRequired();

        $form->addText('points', "Points")
            ->setAttribute('placeholder', 'Achievement points value')
            ->addRule(Form::PATTERN, "Points must contain numbers only.", '^[0-9]+$')
            ->setRequired();

        $form->addSubmit('submit', "Create");

        $form->onSuccess[] = array($this, 'categoryFormSuccess');
        $form->addProtection([$message = "Not allowed"], [$timeout = NULL]);
        return $form;
    }

    // ---------- On submit -------------
    public function categoryFormSuccess($form, $values)
    {
        if(!$this->achievementModel->createAchievement($values)){
            $this->presenter->flashMessage("There was an error during achievement creation, achievement not created", 'warning');
            $this->presenter->redirect('this');
        } else {
            $this->presenter->flashMessage("Achievement successfully created.", 'success');
            $this->presenter->redirect('this');
        }
    }
} 