<?php
/**
 * Created by PhpStorm.
 * User: Ladislav
 * Date: 13. 2. 2015
 * Time: 13:06
 */

namespace App\Controls;

use Nette,
    Nette\Application\UI\Form;

class CategoryForm extends Nette\Object {


    // ---------- Variables ------------
    private $categoryModel;
    private $presenter;

    // ------------ Init ---------------
    public function __construct(\App\AdminModule\Model\CategoryModel $categoryModel)
    {
        $this->categoryModel = $categoryModel;
    }

    // ------------ Form ---------------
    public function create($presenter)
    {
        $this->presenter = $presenter;

        $form = new Form();

        $form->addText('name', "Name")
            ->setAttribute('placeholder', 'Name of category');

        $form->addSubmit('submit', "Create");

        $form->onSuccess[] = array($this, 'categoryFormSuccess');
        $form->addProtection([$message = "Not allowed"], [$timeout = NULL]);
        return $form;
    }

    // ---------- On submit -------------
    public function categoryFormSuccess($form, $values)
    {
        if(!$this->categoryModel->newCategory($values)) {
            $this->presenter->flashMessage("There was an error during category creation, category not created", 'warning');
            $this->presenter->redirect('this');
        } else {
            $this->presenter->flashMessage("Category successfully created.", 'success');
            $this->presenter->redirect('this');
        }
    }
} 