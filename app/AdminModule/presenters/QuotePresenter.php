<?php
/**
 * Created by PhpStorm.
 * User: Ladislav
 * Date: 18. 2. 2015
 * Time: 20:00
 */

namespace App\AdminModule\Presenters;

use Nette;

class QuotePresenter extends BasePresenter {



    // ---------- Variables ------------
    /**
     * @autowire
     * @var \App\Controls\QuoteForm
     */
    protected $quoteFormFactory;
    /**
     * @autowire
     * @var \App\AdminModule\Model\QuoteModel
     */
    protected $quoteModel;


    // ----------- Renders -------------
    public function renderDefault()
    {
        $this->template->quotes = $this->quoteModel->getQuotes();
    }

    /**
     * @param $id
     */
    public function renderDelete($id)
    {
        if(!$this->quoteModel->deleteQuote($id))
        {
            $this->presenter->flashMessage('Can not delete this quote', 'warning');
            $this->redirect('default');
        } else {
            $this->presenter->flashMessage("Quote successfully deleted.", 'success');
            $this->redirect('default');
        }
    }

    // ---------- Component ------------
    public function createComponentQuoteForm()
    {
        return $this->quoteFormFactory->create($this->getPresenter());
    }
} 