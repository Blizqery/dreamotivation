<?php
/**
 * Created by PhpStorm.
 * User: Ladislav
 * Date: 18. 2. 2015
 * Time: 19:11
 */

namespace App\AdminModule\Presenters;

use Nette;

class AchievementPresenter extends BasePresenter {


    // ---------- Variables ------------
    /**
     * @autowire
     * @var \App\Controls\AchievementForm
     */
    protected $achievementFormFactory;
    /**
     * @autowire
     * @var \App\AdminModule\Model\AchievementModel
     */
    protected $achievementModel;
    public    $actionType;


    // ----------- Renders -------------
    public function renderDefault()
    {
        $this->template->achievements = $this->achievementModel->getAchievements();
    }

    /**
     * @param $id
     */
    public function renderDelete($id)
    {
        if(!$this->achievementModel->deleteAchievement($id)) {
            $this->presenter->flashMessage('Can not delete this achievement', 'warning');
            $this->redirect('default');
        } else {
            $this->presenter->flashMessage("Achievement successfully deleted.", 'success');
            $this->redirect('default');
        }
    }

    // ---------- Component ------------
    public function createComponentAchievementForm()
    {
        return $this->achievementFormFactory->create($this->getPresenter());
    }
} 