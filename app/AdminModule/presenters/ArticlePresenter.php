<?php
/**
 * Created by PhpStorm.
 * User: Ladislav
 * Date: 18. 2. 2015
 * Time: 18:58
 */

namespace App\AdminModule\Presenters;

use Nette;

class ArticlePresenter extends BasePresenter {

    // ---------- Variables ------------
    /**
     * @autowire
     * @var \App\Controls\ArticleForm
     */
    protected $articleFormFactory;
    /**
     * @autowire
     * @var \App\AdminModule\Model\ArticleModel
     */
    protected $articleModel;


    // ----------- Renders -------------
    public function renderDefault()
    {
        $this->template->articles = $this->articleModel->allArticles();
    }

    /**
     * @param $id
     */
    public function renderRemove($id)
    {
        if(!$this->articleModel->removeArticle($id)) {
            $this->presenter->flashMessage('Article can not be delete.', 'warning');
            $this->redirect('Article:');
        } else {
            $this->presenter->flashMessage('Article successfully deleted.', 'success');
            $this->redirect('Article:');
        }
    }

    /**
     * @param $id
     * @param $pub
     */
    public function renderPublish($id, $pub)
    {
        $pub = intval($pub);
        if(!$this->articleModel->publishArticle($id, $pub?0:1)) {
            $this->presenter->flashMessage('There was an error publishing article.', 'warning');
            $this->redirect('Article:');
        } else {
            $this->presenter->flashMessage('Article successfully published.', 'success');
            $this->redirect('Article:');
        }
    }

    /**
     * @param $id
     */
    public function renderPreview($id)
    {
        if(!$this->articleModel->getArticle($id)) {
            $this->presenter->flashMessage('Article not found', 'warning');
            $this->redirect('Article:');
        } else {
            $this->template->article = $this->articleModel->getArticle($id);
        }
    }

    // ---------- Component ------------
    public function createComponentArticleForm()
    {
        return $this->articleFormFactory->create($this->user->getIdentity()->username, $this->getPresenter());
    }



} 