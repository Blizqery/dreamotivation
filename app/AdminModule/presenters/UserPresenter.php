<?php
/**
 * Created by PhpStorm.
 * User: Ladislav
 * Date: 15. 2. 2015
 * Time: 17:24
 */

namespace App\AdminModule\Presenters;

use Nette,
    App\AdminModule\Model;

class UserPresenter extends BasePresenter {


    // ---------- Variables ------------
    /**
     * @autowire
     * @var \App\AdminModule\Model\UserModel
     */
    protected  $userModel;


    // ----------- Renders -------------
    public function renderDefault()
    {
        $this->template->users = $this->userModel->getAllUsers();
    }

    /**
     * @param $id
     * @param $role
     */
    public function renderRole($id, $role)
    {
        if($this->userModel->changeRole($id, $role)) {
            $this->flashMessage('User role has been successfully changed.', 'success');
            $this->redirect('User:default');
        } else {
            $this->flashMessage('There was an error changing user role.', 'warning');
            $this->redirect('User:default');
        }
    }

    /**
     * @param $id
     */
    public function renderDelete($id)
    {
         if($this->userModel->deleteUser($id)) {
             $this->flashMessage('User has been successfully deleted.', 'success');
             $this->redirect('User:default');
         } else {
             $this->flashMessage('There was an error with deleting user.', 'warning');
             $this->redirect('User:default');
         }
    }

    /**
     * @param $id
     * @param $active
     */
    public function renderActivate($id, $active)
    {
        $active = intval($active);
        if($this->userModel->activateUser($id, $active?0:1)) {
            $this->flashMessage('User has been successfully activated.', 'success');
            $this->redirect('User:default');
        } else {
            $this->flashMessage('There was an error activating.', 'warning');
            $this->redirect('User:default');
        }
    }
} 