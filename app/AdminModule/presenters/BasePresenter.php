<?php

namespace App\AdminModule\Presenters;

use Nette,
	App\Model;


/**
 * Base presenter for all application presenters.
 */
abstract class BasePresenter extends Nette\Application\UI\Presenter
{
    use \Kdyby\Autowired\AutowireProperties;

    protected function startup()
    {
        parent::startup();

        if (!$this->getUser()->isAllowed($this->name, $this->action)) {
            $this->flashMessage('Permission denied.', 'danger');

            if ($this->name == 'Sign' && $this->action == 'in' || $this->getUser()->isLoggedIn()) {
                $this->redirect(':Frontend:Homepage:');
            } else {
                $this->redirect(':Frontend:Sign:in');
            }
        }
    }
}
