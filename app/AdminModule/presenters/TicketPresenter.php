<?php
/**
 * Created by PhpStorm.
 * User: Ladislav
 * Date: 18. 2. 2015
 * Time: 21:57
 */

namespace App\AdminModule\Presenters;

use Nette;

class TicketPresenter extends BasePresenter {


    // ---------- Variables ------------
    /**
     * @autowire
     * @var \App\AdminModule\Model\TicketModel
     */
    protected  $ticketModel;

    // ----------- Renders -------------
    public function renderDefault()
    {
        $this->template->tickets = $this->ticketModel->getTickets();
    }

    /**
     * @param $id
     */
    public function renderDone($id)
    {
        $this->ticketModel->ticketSolved($id);
        $this->flashMessage('Ticket has been solved', 'success');
        $this->redirect('Ticket:');
    }
} 