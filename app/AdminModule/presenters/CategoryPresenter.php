<?php
/**
 * Created by PhpStorm.
 * User: Ladislav
 * Date: 13. 2. 2015
 * Time: 12:35
 */

namespace App\AdminModule\Presenters;

use Nette,
    App\AdminModule\Model;

class CategoryPresenter extends BasePresenter {


    // ---------- Variables ------------
    /**
     * @autowire
     * @var \App\Controls\CategoryForm
     */
    protected $categoryFormFactory;
    /**
     * @autowire
     * @var \App\AdminModule\Model\CategoryModel
     */
    protected $categoryModel;
    public    $actionType;


    // ----------- Renders -------------
    public function renderDefault()
    {
        $id = $this->getParameter('id');
        $actionType = $this->getParameter('type');

        if(isset($id, $actionType) && $actionType == "rem"){
            if($this->categoryModel->removeCategory($id)){
                $this->flashMessage('Category successfully deleted', 'success');
                $this->redirect('this');
            } else {
                $this->flashMessage('Category is not empty, cannot be deleted.', 'warning');
                $this->redirect('this');
            }

        }

        $this->template->categories = $this->categoryModel->getAllCategories();
        $this->template->count = $this->categoryModel->articleCount();
    }

    // ---------- Component ------------
    public function createComponentCategoryForm()
    {
        return $this->categoryFormFactory->create($this->getPresenter());
    }
} 