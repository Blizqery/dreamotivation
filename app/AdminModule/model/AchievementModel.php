<?php
/**
 * Created by PhpStorm.
 * User: Ladislav
 * Date: 18. 2. 2015
 * Time: 19:12
 */

namespace App\AdminModule\Model;

use Nette;

class AchievementModel extends BaseModel{

    /**
     * @param $values
     * @return bool|int|Nette\Database\Table\IRow
     */
    public function createAchievement($values)
    {
        try {
            return $this->getTable()->insert(array(
                'name'      => $values->name,
                'describe'  => $values->describe,
                'points'    => $values->points
            ));
        } catch(\PDOException $e) {
            return 0;
        }

    }

    /**
     * @param $id
     * @return int
     */
    public function deleteAchievement($id)
    {
        try {
            return $this->getTable()->where('id_achievement', $id)->delete();
        } catch(\PDOException $e) {
            return 0;
        }
    }

    /**
     * @return Nette\Database\Table\Selection
     */
    public function getAchievements()
    {
        return $this->getTable()->select('*');
    }

} 