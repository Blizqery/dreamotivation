<?php
/**
 * Created by PhpStorm.
 * User: Ladislav
 * Date: 18. 2. 2015
 * Time: 21:49
 */

namespace App\AdminModule\Model;

use Nette;

class TicketModel extends BaseModel {

    /**
     * @return Nette\Database\Table\Selection
     */
    public function getTickets()
    {
        return $this->getTable()->select('*')->order('date_sent');
    }

    /**
     * @param $id
     * @return int
     */
    public function ticketSolved($id)
    {
        return $this->getTable()->where('id_ticket', $id)->update(array('done' => 1));
    }
} 