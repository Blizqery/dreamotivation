<?php
/**
 * Created by PhpStorm.
 * User: Ladislav
 * Date: 18. 2. 2015
 * Time: 20:00
 */

namespace App\AdminModule\Model;

use Nette;

class QuoteModel extends BaseModel {

    /**
     * @param $values
     * @return bool|int|Nette\Database\Table\IRow
     */
    public function createQuote($values)
    {
        try {
            return $this->getTable()->insert(array(
                    'text'   =>  $values->text,
                    'author' =>  $values->author,
            ));
        } catch(\PDOException $e) {
            return 0;
        }
    }

    /**
     * @return Nette\Database\Table\Selection
     */
    public function getQuotes()
    {
        return $this->getTable()->select('*');
    }

    /**
     * @param $id
     * @return int
     */
    public function deleteQuote($id)
    {
        try {
            return $this->getTable()->where('id_quote', $id)->delete();
        } catch(\PDOException $e) {
            return 0;
        }
    }
} 