<?php
/**
 * Created by PhpStorm.
 * User: Ladislav
 * Date: 18. 2. 2015
 * Time: 19:58
 */

namespace App\AdminModule\Model;

use Nette;

class ArticleModel extends BaseModel
{

    /**
     * @param $values
     * @return bool|int|Nette\Database\Table\IRow
     */
    public function createArticle($values)
    {
        try {
            return $this->getTable()->insert(array(
                'title' => $values->title,
                'summary' => $values->summary,
                'author' => $values->author,
                'created_at' => new Nette\Database\SqlLiteral('NOW()'),
                'content' => $values->text,
                'source' => $values->source,
                'source_name' => $values->source_name,
                'article_category_id' => $values->article_category_id
            ));
        } catch (\PDOException $e) {
            return 0;
        }
    }

    /**
     * @return Nette\Database\Table\Selection
     */
    public function allArticles()
    {
        return $this->getTable()->select('*');
    }

    /**
     * @param $id
     * @return int|Nette\Database\Table\Selection
     */
    public function getArticle($id)
    {
        try {
            return $this->getTable()->where('id_article', $id)->select('*');
        } catch(\PDOException $e) {
            return 0;
        }
    }

    /**
     * @param $id
     * @param $pub
     * @return int
     */
    public function publishArticle($id, $pub)
    {
        try {
            return $this->getTable()->where('id_article', $id)->update(array('published' => $pub));
        } catch(\PDOException $e) {
            return 0;
        }
    }

    /**
     * @param $id
     * @return int
     */
    public function removeArticle($id)
    {
        try {
            return $this->getTable()->where('id_article', $id)->delete();
        } catch(\PDOException $e) {
            return 0;
        }
    }
} 