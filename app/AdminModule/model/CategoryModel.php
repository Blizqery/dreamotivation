<?php
/**
 * Created by PhpStorm.
 * User: Ladislav
 * Date: 13. 2. 2015
 * Time: 13:08
 */

namespace App\AdminModule\Model;

use Nette;

class CategoryModel extends BaseModel {

    /**
     * @return array
     */
    public function articleCount()
    {
        $ret = $this->getTableByName('article')->select('article_category_id')->order('article_category_id');

        $categoryArticles = [];
        $prev = "";

        foreach($ret as $r){
            if($r['article_category_id'] === $prev){
                $categoryArticles[$r['article_category_id']] += 1;
            } else {
                $categoryArticles[$r['article_category_id']] = 1;
                $prev = $r['article_category_id'];
            }
        }

        return $categoryArticles;
    }

    /**
     * @param $id
     * @return int
     */
    public function removeCategory($id)
    {
        try {
            return $this->getTableByName('article_category')->where('id_article_category', $id)->delete();
        } catch(\PDOException $e) {
            return 0;
        }

    }

    /**
     * @param $id
     * @param $name
     * @return int
     */
    public function editCategory($id, $name)
    {
        return $this->getTableByName('article_category')->where('id_article_category', $id)->update('name', $name);
    }

    public function getAllCategories()
    {
        return $this->getTableByName('article_category')->select('*')->order('id_article_category');
    }

    public function newCategory($values)
    {
        try {
            return $this->getTableByName('article_category')->insert(array(
                'name' => $values->name,
            ));
        } catch(\PDOException $e) {
            return 0;
        }
    }
} 