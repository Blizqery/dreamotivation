<?php
/**
 * Created by PhpStorm.
 * User: Ladislav
 * Date: 11. 2. 2015
 * Time: 10:58
 */

namespace App\AdminModule\Model;

use Nette;

class BaseModel extends Nette\Object {

    // ---------- Variables ------------
    private $database;


    // ------------ Init ---------------
    public function __construct(Nette\Database\Context $database)
    {
        $this->database = $database;
    }

    // -------- Getting tables ---------
    /**
     * @return Nette\Database\Table\Selection
     */
    public function getTable()
    {
        preg_match('#(\w+)Model$#', get_class($this), $m);
        return $this->database->table(lcfirst($m[1]));
    }

    /**
     * @param $name
     * @return Nette\Database\Table\Selection
     */
    public function getTableByName($name)
    {
        return $this->database->table($name);
    }
} 