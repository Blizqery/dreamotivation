<?php
/**
 * Created by PhpStorm.
 * User: Ladislav
 * Date: 15. 2. 2015
 * Time: 17:29
 */

namespace App\AdminModule\Model;

use Nette;

class UserModel extends BaseModel {

    /**
     * @return Nette\Database\Table\Selection
     */
    public function getAllUsers()
    {
        return $this->getTable()->select('*')->order('role, username');
    }

    /**
     * @param $active 1 for activate 0 for deactivate
     * @param $id user's id
     */
    public function activateUser($id, $active)
    {
        return $this->getTable()->wherePrimary($id)->update(array('activated' => $active));
    }

    /**
     * @param $id
     * @return int
     */
    public function deleteUser($id)
    {
        return $this->getTable()->wherePrimary($id)->delete();
    }

    /**
     * @param $id
     * @param $role
     * @return int
     */
    public function changeRole($id, $role)
    {
        return $this->getTable()->wherePrimary($id)->update(array('role' => $role));
    }
} 