<?php

namespace App;

use Nette,
	Nette\Application\Routers\RouteList,
	Nette\Application\Routers\Route,
	Nette\Application\Routers\SimpleRouter;


/**
 * Router factory.
 */
class RouterFactory
{

	/**
	 * @return \Nette\Application\IRouter
	 */
	public static function createRouter()
	{
		$router = new RouteList();

        /* AdminModule */
        $router[] = $admin = new RouteList('Admin');
        $admin[] = new Route('admin/<presenter>/<action>[/<id>]', 'Admin:Administration:default');

        /* FrontendModule */
        $router[] = $frontend = new RouteList('Frontend');
		$frontend[] = new Route('<presenter>/<action>[/<id>]', 'Homepage:default');

        return $router;
	}

}
