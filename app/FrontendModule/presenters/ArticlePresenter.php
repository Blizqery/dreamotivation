<?php
/**
 * Created by PhpStorm.
 * User: Ladislav
 * Date: 18. 3. 2015
 * Time: 15:40
 */

namespace App\FrontendModule\Presenters;

use Nette;

class ArticlePresenter extends BasePresenter
{


    // ---------- Variables ------------
    /**
     * @autowire
     * @var \App\FrontendModule\Model\ArticleModel
     */
    protected $articleModel;


    // ----------- Renders -------------
    /**
     * @param $id
     */
    public function renderDetail($id)
    {
        $article = $this->articleModel->getArticleById($id);

        if(!$article) {
            $this->presenter->flashMessage("Article does not exist.", "warning");
            $this->redirect("Homepage:");
        } else {
            $this->template->article = $article;
        }
    }
} 