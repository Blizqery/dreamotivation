<?php
/**
 * Created by PhpStorm.
 * User: Ladislav
 * Date: 11. 2. 2015
 * Time: 12:48
 */

namespace App\FrontendModule\Presenters;

use Nette,
    App\FrontendModule\Model;

class RegisterPresenter extends BasePresenter {

    // ---------- Variables ------------
    /**
     * @autowire
     * @var \App\Controls\RegisterForm
     */
    protected $registerFormFactory;


    // ---------- Component ------------

    /**
     * @return Nette\Application\UI\Form
     */
    public function createComponentRegisterForm()
    {
        return $this->registerFormFactory->create($this->getPresenter());
    }
} 