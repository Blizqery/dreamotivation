<?php

namespace App\FrontendModule\Presenters;

use Nette,
    App\FrontendModule\Model;


/**
 * Sign in/out presenters.
 */
class SignPresenter extends BasePresenter
{

    /**
     * @autowire
     * @var Model\RegisterModel
     */
    protected $registerModel;


    public function renderOut()
    {
        if ($this->getUser()->isLoggedIn()) {
            $this->actionOut();
        }
    }

	/**
	 * Sign-in form factory.
	 * @return Nette\Application\UI\Form
	 */
	protected function createComponentSignInForm()
	{
		$form = new Nette\Application\UI\Form;
		$form->addText('username', 'Username:')
			->setRequired('Please enter your username.');

		$form->addPassword('password', 'Password:')
			->setRequired('Please enter your password.');

		$form->addCheckbox('remember', 'Keep me signed in');

		$form->addSubmit('send', 'Sign in');

		// call method signInFormSucceeded() on success
		$form->onSuccess[] = array($this, 'signInFormSucceeded');
		return $form;
	}


	public function signInFormSucceeded($form, $values)
	{
		if ($values->remember) {
			$this->getUser()->setExpiration('14 days', FALSE);
		} else {
			$this->getUser()->setExpiration('20 minutes', TRUE);
		}

		try {
			$this->getUser()->login($values->username, $values->password);
            $this->registerModel->updateOnline($values->username);
			$this->redirect('Homepage:');

		} catch (Nette\Security\AuthenticationException $e) {
			$form->addError($e->getMessage());
		}
	}


	public function actionOut()
	{
		$this->getUser()->logout();
        $this->getSession()->destroy();
        $this->flashMessage('You have been signed out.', 'success');
		$this->redirect('in');
	}

}
