<?php

namespace App\FrontendModule\Presenters;

use Nette,
	App\FrontendModule\Model;


/**
 * Homepage presenter.
 */
class HomepagePresenter extends BasePresenter
{

    // ---------- Variables ------------
    /**
     * @autowire
     * @var Model\ArticleModel
     */
    protected $articleModel;
    /**
     * @autowire
     * @var Model\CategoryModel
     */
    protected $categoryModel;
    private $line = 5;
    private $id;


    // ----------- Renders -------------
    public function renderDefault()
    {
        $categories = $this->categoryModel->getHomepageCategories();
        $articles = $this->articleModel->getHomepageArticles();

        $this->template->articles = $articles;
        $this->template->id = $this->id;
        $this->template->line = $this->line;
        $this->template->categories = $categories;
    }

    /**
     * @param $id
     */
    public function handleShow($id)
    {
        $this->id = $id;
        $this->redrawControl('articles');
    }
}
