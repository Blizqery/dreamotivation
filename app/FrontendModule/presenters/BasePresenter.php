<?php

namespace App\FrontendModule\Presenters;

use Nette,
	App\FrontendModule\Model;


/**
 * Base presenter for all application presenters.
 */
abstract class BasePresenter extends Nette\Application\UI\Presenter
{
    use \Kdyby\Autowired\AutowireProperties;

    // ---------- Variables ------------
    /**
     * @autowire
     * @var \App\FrontendModule\Model\QuoteModel
     */
    public $quoteModel;



    protected function startup()
    {
        parent::startup();

        if (!$this->getUser()->isAllowed($this->name, $this->action)) {
            $this->flashMessage('Permission denied.', 'danger');

            if ($this->name == 'Sign' && $this->action == 'in' || $this->getUser()->isLoggedIn()) {
                $this->redirect('Homepage:');
            } else {
                $this->redirect('Sign:in');
            }
        }
    }

    // ----------- Renders -------------
    protected function beforeRender()
    {
        parent::beforeRender();
        $quotes = $this->quoteModel->getQuotes();
        $this->template->quotes = $quotes[rand(0, count($quotes)-1)];
    }

}
