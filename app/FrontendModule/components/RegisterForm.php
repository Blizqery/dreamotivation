<?php
/**
 * Created by PhpStorm.
 * User: Ladislav
 * Date: 11. 2. 2015
 * Time: 11:50
 */

namespace App\Controls;

use Nette,
    Nette\Application\UI\Form;

class RegisterForm extends Nette\Object {


    // ---------- Variables ------------
    private $registerModel;
    private $presenter;
    public  $onRegistered;

    // ------------ Init ---------------

    public function __construct(\App\FrontendModule\Model\RegisterModel $registerModel)
    {
        $this->registerModel = $registerModel;
    }

    // ------------ Form ---------------
    public function create($presenter)
    {
        $this->presenter = $presenter;

        $form = new Form();

        $form->addText('username', "Username")
            ->setAttribute('placeholder', 'Nickname')
            ->addRule(Form::PATTERN, "Username must contain letters and numbers only.", '^[a-zA-Z][a-zA-Z0-9]+$')
            ->setRequired("Please enter your new Nickname.");

        $form->addPassword('password', "Password")
            ->setAttribute('placeholder', 'Password')
            ->setRequired("Please enter your password");
        $form->addPassword('passwordVerify', "Password again")
            ->setAttribute('placeholder', 'Password again')
            ->setRequired("Please enter your password again.")
            ->addRule(Form::EQUAL, 'Passwords do not match.', $form['password']);

        $form->addText('email', "Email")
            ->setAttribute('placeholder', 'Email')
            ->addRule(Form::EMAIL, "Please enter your email.")
            ->setRequired("Please enter you email");

        $form->addText('first_name', "First name")
            ->setAttribute('placeholder', 'First name');

        $form->addSubmit('submit', "Register");

        $form->onSuccess[] = array($this, 'registerFormSuccess');
        $form->addProtection([$message = "Not allowed"], [$timeout = NULL]);
        return $form;
    }

    // ---------- On submit -------------
    public function registerFormSuccess($form, $values)
    {
        unset($values['passwordVerify']);

        if(!$this->registerModel->register($values)) {
            $this->presenter->flashMessage("There was an error during registration, user not created", 'warning');
            $this->presenter->redirect('Register:');
        } else {
            $this->presenter->flashMessage("User successfully registered.", 'success');
            $this->presenter->getUser()->login($values->username, $values->password);
            $this->registerModel->updateOnline($values->username);
            $this->presenter->redirect('Homepage:');
        }

    }
} 