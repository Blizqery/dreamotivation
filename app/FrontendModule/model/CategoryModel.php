<?php
/**
 * Created by PhpStorm.
 * User: Ladislav
 * Date: 17. 3. 2015
 * Time: 14:15
 */

namespace App\FrontendModule\Model;

use Nette;

class CategoryModel extends BaseModel {

    /**
     * @return Nette\Database\Table\Selection
     */
    public function getHomepageCategories()
    {
        return $this->getTablebyName('article_category')->select('*')->limit(4);
    }

} 