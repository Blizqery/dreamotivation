<?php
/**
 * Created by PhpStorm.
 * User: Ladislav
 * Date: 17. 3. 2015
 * Time: 14:17
 */

namespace App\FrontendModule\Model;

use Nette;


class ArticleModel extends BaseModel {

    /**
     * @return array
     */
    public function getHomepageArticles()
    {
        $res = array();
        for($i = 1; $i <=4; $i++) {
           $ars = $this->getTable()->where("article_category_id = $i")->where("published = 1")->select("*")->order("created_at DESC")->limit(15)->fetchAll();
           $res = array_merge($ars, $res);
        }

        return $res;
    }

    /**
     * @param $articleId
     * @return bool|int|mixed|Nette\Database\Table\IRow
     */
    public function getArticleById($articleId)
    {
        try {
            return $this->getTable()->where('id_article', $articleId)->select('*')->fetch();
        } catch(\PDOException $e) {
            return 0;
        }
    }
} 