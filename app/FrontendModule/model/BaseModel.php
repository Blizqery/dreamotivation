<?php
/**
 * Created by PhpStorm.
 * User: Ladislav
 * Date: 11. 2. 2015
 * Time: 10:58
 */

namespace App\FrontendModule\Model;

use Nette;

class BaseModel extends Nette\Object {

    // ---------- Variables ------------
    public $database;


    // ------------ Init ---------------
    public function __construct(Nette\Database\Context $database)
    {
        $this->database = $database;
    }

    // -------- Getting tables ---------
    public function getTable()
    {
        preg_match('#(\w+)Model$#', get_class($this), $m);
        return $this->database->table(lcfirst($m[1]));
    }

    public function getTableByName($name)
    {
        return $this->database->table($name);
    }
} 