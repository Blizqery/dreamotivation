<?php

namespace App;

use Nette\Security as NS;


class Authorizator extends NS\Permission implements NS\IAuthorizator
{
    // User roles
    const GUEST = 'guest';
    const USER = 'user';
    const ADMIN = 'admin';
    const MODERATOR = 'moderator';

    /**
     * Construct
     */
    public function __construct()
    {
        $this->setRoles();
        $this->setResources();
        $this->denied();
        $this->allowed();
    }

    private function allowed()
    {
        // Guest
        $this->allow(self::GUEST, array(
            'Frontend:Sign',
            'Frontend:Homepage',
            'Frontend:Register',
            'Frontend:Article',
        ));

        // User
        $this->allow(self::USER, array(

        ));

        $this->allow(self::MODERATOR, array(
            'Admin:Homepage',
            'Admin:Article',
            'Admin:Quote',
            'Admin:Achievement',
        ));

        // Administrator
        $this->allow(self::ADMIN, array(
            'Admin:Category',
            'Admin:Homepage',
            'Admin:User',
            'Admin:Achievement',
            'Admin:Ticket'
        ));
    }

    private function denied()
    {
        // Guest can't access these pages
        $this->deny(self::GUEST, 'Admin:Homepage');
        $this->deny(self::GUEST, 'Admin:Category');
        $this->deny(self::GUEST, 'Admin:User');
        $this->deny(self::GUEST, 'Admin:Achievement');
        $this->deny(self::GUEST, 'Admin:Article');
        $this->deny(self::GUEST, 'Admin:Quote');
        $this->deny(self::GUEST, 'Admin:Ticket');

        // Logged users can't access these pages
        $this->deny(self::USER, 'Frontend:Sign', 'in');
        $this->deny(self::USER, 'Frontend:Register');


    }

    /**
     * Role definition
     */
    private function setRoles()
    {
        $this->addRole(self::GUEST);
        $this->addRole(self::USER, self::GUEST);
        $this->addRole(self::MODERATOR, self::USER);
        $this->addRole(self::ADMIN, self::MODERATOR);
    }

    /**
     * Resource definition
     */
    private function setResources()
    {
        // Frontend
        $this->addResource('Frontend:Register');
        $this->addResource('Frontend:Homepage');
        $this->addResource('Frontend:Sign');
        $this->addResource('Frontend:Article');

        // Administration
        $this->addResource('Admin:Homepage');
        $this->addResource('Admin:Category');
        $this->addResource('Admin:User');
        $this->addResource('Admin:Article');
        $this->addResource('Admin:Achievement');
        $this->addResource('Admin:Quote');
        $this->addResource('Admin:Ticket');
    }
}
