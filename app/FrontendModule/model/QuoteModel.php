<?php
/**
 * Created by PhpStorm.
 * User: Ladislav
 * Date: 17. 3. 2015
 * Time: 14:00
 */

namespace App\FrontendModule\Model;

use Nette;

class QuoteModel extends BaseModel {

    /**
     * @return Nette\Database\Table\Selection
     */
    public function getQuotes()
    {
        return $this->getTable()->select('*');
    }
} 