<?php
/**
 * Created by PhpStorm.
 * User: Ladislav
 * Date: 11. 2. 2015
 * Time: 11:51
 */

namespace App\FrontendModule\Model;

use Nette,
    Nette\Security\Passwords;

class RegisterModel extends BaseModel {

    /**
     * @param $values
     * @return bool|int|Nette\Database\Table\IRow
     */
    public function register($values)
    {
        try {
            return $this->getTableByName('user')->insert(array(
                'username'          => $values->username,
                'role'          => "user",
                'email'         => $values->email,
                'password'      => Passwords::hash($values->password),
                'first_name'    => $values->first_name,
                'registered_at' => new Nette\Database\SqlLiteral('NOW()')
            ));
        } catch (\PDOException $e) {
            return 0;
        }
    }

    /**
     * @param $username
     * @return int
     */
    public function updateOnline($username)
    {
        return $this->getTableByName('user')->where('username', $username)->update(array('last_online' => new Nette\Database\SqlLiteral('NOW()')));
    }
} 